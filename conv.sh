#!/bin/bash
calc_size() {
  echo $(sed -En 's|.*=([[:digit:]]+):([[:digit:]]+):.*|\1*\2|p'<<<"$1" | bc)
}
IFS='
'
probe=ffprobe
conv=ffmpeg
#codec="-c:v libx264 -crf 20"
codec="-c:v libx265 -crf 20"
#debug=true
if [ -d "../Projects" ]; then
  outputdir="../Projects/"
else
  outputdir="./"
fi
for folder in $( ls -d1 */ ) ; do
  if [ -d "$folder" ]; then
    echo "Working on $folder"
    counter=0
    #cd "$folder"
    for file in $( ls -rc1 "${folder}"/*.mkv ) ; do
      echo " encoding $file"
      if [[ $counter -gt 0 ]]; then
        printf -v count "%02d" $counter
        outputname="${folder}_${counter}.mkv"
      else
        outputname="${folder}.mkv"
      fi
      outputname=$( echo "$outputname" | sed 's/_/ /g; s/.*/\L&/; s/[a-z]*/\u&/g; s/\( And \| The \| A \| An \| Is \| Or \| Nor \| For \| But \|Mkv$\)/\L&/g; s/^./\u&/' )
      counter=$(($counter + 1))
      filenoext=$( echo $file | sed  's/\(mkv$\|mp4$\|avi$\|mov$\)//' )
      #find the mapping of the Audio and Video streams
      mapv=$( $probe "$file" 2>&1 | grep Video | cut -d '#' -f 2 | cut -d '(' -f 1 )
      mapa=$( $probe "$file" 2>&1 | grep Audio | cut -d '#' -f 2 | cut -d '(' -f 1 )
      map="-map $mapv"
      for track in $mapa;do
        map="$map -map $track"
      done
      echo "map: $map"
      # find the total duration in seconds
      duration=$($probe "$file" -show_format 2>&1| sed -En '/duration/s|.*=(.*$)|\1|p')
      time1=$(bc <<< "scale=2; $duration/10")
      time2=$(bc <<< "scale=2; $duration/2")
      time3=$(bc <<< "scale=2; $duration*0.8")
      if [ "x$debug" = "xtrue" ]; then echo "1: $time1 2: $time2 3: $time3"; fi
      #find the cropping parameters to crop away black edges, test three time points and pick the largest cropping area
      crop1=$( $conv -ss $time1 -i "$file" -t 1 -vf cropdetect -f null - 2>&1 | awk '/crop/ { print $NF }' | tail -1 )
      size1=$(calc_size "$crop1")
      crop2=$( $conv -ss $time2 -i "$file" -t 1 -vf cropdetect -f null - 2>&1 | awk '/crop/ { print $NF }' | tail -1 )
      size2=$(calc_size "$crop2")
      crop3=$( $conv -ss $time3 -i "$file" -t 1 -vf cropdetect -f null - 2>&1 | awk '/crop/ { print $NF }' | tail -1 )
      size3=$(calc_size "$crop3")
      crop=$crop3
      if [ "x$debug" = "xtrue" ]; then echo "1: $crop1 2: $crop2 3: $crop3"; fi
      if [ "x$debug" = "xtrue" ]; then echo "1: $size1 2: $size2 3: $size3"; fi
      if [ $size1 -gt $size2 ]; then
        if [ $size1 -gt $size3 ]; then
          crop=$crop1
        fi
      else
        if [ $size2 -gt $size3 ]; then
          crop=$crop2
        fi
      fi
      echo "crop: $crop"
      if [ ! -s "${filenoext}conv.mkv" ]; then
        eval $conv -hide_banner -loglevel error -stats -i \"$file\" $map -vf \"$crop, yadif\" $codec -c:a copy -sn \"${filenoext}conv.mkv\" || continue
      fi
      mkvmerge -o "${outputdir}$outputname" -T --no-global-tags --no-chapters -M "${filenoext}conv.mkv" -A -D "$file" || continue
      mkvmerge --identify "$file"
      mkvmerge --identify "${outputdir}$outputname" || continue
      rm "${filenoext}conv.mkv"
    done
  fi
done
