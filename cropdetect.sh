#!/bin/bash
calc_size() {
  echo $(sed -En 's|.*=([[:digit:]]+):([[:digit:]]+):.*|\1*\2|p'<<<"$1" | bc)
}
file=$1
# find the total duration in seconds
duration=$(ffprobe "$file" -show_format 2>&1| sed -En '/duration/s|.*=(.*$)|\1|p')
time1=$(bc <<< "scale=2; $duration/10")
time2=$(bc <<< "scale=2; $duration/2")
time3=$(bc <<< "scale=2; $duration*0.8")
if [ "x$debug" = "xtrue" ]; then echo "1: $time1 2: $time2 3: $time3"; fi
#find the cropping parameters to crop away black edges, test three time points and pick the largest cropping area
crop1=$( ffmpeg -ss $time1 -i "$file" -t 1 -vf cropdetect -f null - 2>&1 | awk '/crop/ { print $NF }' | tail -1 )
size1=$(calc_size "$crop1")
crop2=$( ffmpeg -ss $time2 -i "$file" -t 1 -vf cropdetect -f null - 2>&1 | awk '/crop/ { print $NF }' | tail -1 )
size2=$(calc_size "$crop2")
crop3=$( ffmpeg -ss $time3 -i "$file" -t 1 -vf cropdetect -f null - 2>&1 | awk '/crop/ { print $NF }' | tail -1 )
size3=$(calc_size "$crop3")
crop=$crop3
if [ "x$debug" = "xtrue" ]; then echo "1: $crop1 2: $crop2 3: $crop3"; fi
if [ "x$debug" = "xtrue" ]; then echo "1: $size1 2: $size2 3: $size3"; fi
if [ $size1 -gt $size2 ]; then
if [ $size1 -gt $size3 ]; then
    crop=$crop1
fi
else
if [ $size2 -gt $size3 ]; then
    crop=$crop2
fi
fi
echo "crop: $crop"
