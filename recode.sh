#!/bin/bash
IFS='
'
conv=ffmpeg
probe=ffprobe
#vc="-c:v libx264 -crf 20"
vc="-c:v libx265 -crf 23"
#vc="-c:v hevc_nvenc -rc vbr_hq -cq 26"
ac="-c:a aac -ac 6"
sc="-c:s copy"
filter="scale='min(1920,iw)':min'(1080,ih)':force_original_aspect_ratio=decrease,pad=1920:1080:(ow-iw)/2:(oh-ih)/2"
outputdir=/media/${USER}/Movies
recode() {
    file=$1
    outputname=$2
    mopt=""
    if [ -f "${file::-3}en.srt" ]; then
        mopt="--language 0:en \"${file::-3}en.srt\""
    fi
    if [ -f "${file::-3}de.srt" ]; then
        mopt="$mopt --language 0:de \"${file::-3}de.srt\""
    fi
    if [ -f "${file::-3}hr.srt" ]; then
        mopt="$mopt --language 0:hr \"${file::-3}hr.srt\""
    fi
    touch "$outputdir/$outputname".inprogress
    eval "$conv -analyzeduration 2147483647 -probesize 2147483647 -i \"$file\" $vc $ac $sc -map 0 \"/tmp/$outputname\" && mkvmerge -o \"$outputdir/$outputname\" \"/tmp/$outputname\" $mopt && rm \"/tmp/$outputname\" && rm \"$outputdir/$outputname\".inprogress"
}
for file in $(ls -1 *.{mkv,avi,mp4,mpg,m4v,mov,ogg}); do
    outputname="${file::-3}mkv"
    if [ ! -f "$outputdir/$outputname" ] && [ ! -f "$outputdir/$outputname".inprogress ]; then
        echo ""
        echo ""
        echo Recoding: $file
        #cp "$outputdir/$outputname" ./
        recode $file $outputname
    fi
done