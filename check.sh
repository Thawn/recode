#!/bin/bash
#!/bin/bash
IFS='
'
outputdir=/media/${USER}/Movies
total=0
ok=0
missing=0
wrong_time=0
compare() {
    ((total+=1))
    orig=$1
    target=$2
    if [ -f "$target" ]; then
        os=$( ffprobe -v error -show_entries format=duration -of default=noprint_wrappers=1:nokey=1 "$orig" | sed 's/\.[[:digit:]]*//' )
        ts=$( ffprobe -v error -show_entries format=duration -of default=noprint_wrappers=1:nokey=1 "$target" | sed 's/\.[[:digit:]]*//' )
        osiz=$(stat -c%s "$orig")
        tsiz=$(stat -c%s "$target")
        percent=$(($tsiz * 100 / $osiz))
        if [ $percent -gt 80 ]; then
            echo warning new size is $percent % of original size for file: $orig
        fi
        if [ "$ts" != "N/A" ] && [ "x$ts" != "x" ]; then
            df=$(( $os - $ts ))
            if [ ${df#-} -lt 10 ]; then
                ((ok+=1))
                echo OK $ok 
            else
                ((wrong_time+=1))
                echo Not OK $wrong_time: duration $os != $ts for file: $orig
            fi
        else
            ((wrong_time+=1))
            echo Not OK $wrong_time: could not read time from $target
        fi
    else
        ((missing+=1))
        echo Not OK $missing: file not found: $target
    fi
}
for file in $(ls -1 *.{mkv,avi,mp4,mpg,m4v,mov,ogg}); do
    outputname="${file::-3}mkv"
    compare $file "$outputdir/$outputname"
done
echo total: $total
echo o.k.: $ok
echo missing: $missing
echo wrong time: $wrong_time
echo percent done: $(( $ok * 1000 / $total / 10 ))
date